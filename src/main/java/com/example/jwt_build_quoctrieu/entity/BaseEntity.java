package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "update_by")
    protected Integer  updateBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "create_date")
    protected Date  createDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "update_date")
    protected Date  updateDate;

    @Column(name = "is_delete", columnDefinition = "TINYINT")
    protected int is_Delete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getIs_Delete() {
        return is_Delete;
    }

    public void setIs_Delete(int is_Delete) {
        this.is_Delete = is_Delete;
    }

}
