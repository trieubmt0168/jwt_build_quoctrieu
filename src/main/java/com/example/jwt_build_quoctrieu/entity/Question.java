package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "question")
public class Question extends BaseEntity {

    @Column(name = "question_name")
    private String questionName;

    @Column(name = "create_by")
    private Integer createBy;


    @ManyToMany
    @JoinTable(name = "test_question", joinColumns = {@JoinColumn(name = "id_question")}, inverseJoinColumns = {
            @JoinColumn(name = "id_test")})
    private List<Test> testList = new ArrayList();

    @ManyToOne
    @JoinColumn(name = "id_category")
    private Category category;

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }
}
