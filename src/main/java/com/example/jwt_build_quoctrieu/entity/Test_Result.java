package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;

@Entity
@Table(name = "test_result")
public class Test_Result extends BaseEntity {
    @Column(name = "true_answer")
    private int trueAnswer;

    @Column(name = "number_answer")
    private int numberAnswer;

    @Column(name = "evaluate",length = 30)
    private String evaluate;

    @Column(name = "create_by")
    private Integer createBy;

    @ManyToOne
    @JoinColumn(name = "id_test")
    private Test test;

    public int getTrueAnswer() {
        return trueAnswer;
    }

    public void setTrueAnswer(int trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public int getNumberAnswer() {
        return numberAnswer;
    }

    public void setNumberAnswer(int numberAnswer) {
        this.numberAnswer = numberAnswer;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
