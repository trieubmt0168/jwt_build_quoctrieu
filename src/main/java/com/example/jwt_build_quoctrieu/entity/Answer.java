package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer extends BaseEntity {
    @Column(name = "answer_name")
    private String answerName;

    @Column(name = "is_true", columnDefinition = "TINYINT")
    private int isTrue;

    @Column(name = "create_by")
    private Integer createBy;


    @ManyToOne
    @JoinColumn(name = "id_question")
    private Question question;

    public String getAnswerName() {
        return answerName;
    }

    public void setAnswerName(String answerName) {
        this.answerName = answerName;
    }

    public int getIsTrue() {
        return isTrue;
    }

    public void setIsTrue(int isTrue) {
        this.isTrue = isTrue;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
