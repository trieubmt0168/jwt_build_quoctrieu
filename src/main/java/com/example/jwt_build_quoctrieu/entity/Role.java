package com.example.jwt_build_quoctrieu.entity;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role extends BaseEntity {
    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(name = "role_name")
    private ERole name;
    @Column(name = "create_by")
    private Integer createBy;

    public Role() {

    }

    public Role(ERole name) {
        this.name = name;
    }

    public ERole getName() {
        return name;
    }

//    public void setName(ERole name) {
//        this.name = name;
//    }

}
