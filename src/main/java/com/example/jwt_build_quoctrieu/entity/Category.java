package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "category")
public class Category extends BaseEntity {
    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "description")
    private String description;

    @Column(name = "create_by")
    private Integer createBy;

    @Column(name = "id_parent")
    private Long idParent;

    @OneToMany( fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @JoinColumn(name = "id_parent", insertable = false, updatable = false)
    private Set<Category> parent;


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Long getIdParent() {
        return idParent;
    }

    public void setIdParent(Long idParent) {
        this.idParent = idParent;
    }

    public Set<Category> getParent() {
        return parent;
    }

    public void setParent(Set<Category> parent) {
        this.parent = parent;
    }
}
