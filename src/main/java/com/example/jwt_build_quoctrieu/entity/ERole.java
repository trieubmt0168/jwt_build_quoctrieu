package com.example.jwt_build_quoctrieu.entity;

public enum ERole {
    ROLE_USER,
    ROLE_STUDY,
    ROLE_ADMIN
}