package com.example.jwt_build_quoctrieu.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "test")
public class Test  extends  BaseEntity{
    @Column(name = "test_name",length = 30)
    private String testName;

    @Column(name = "description",length = 30)
    private String description;

    @Column(name = "number_answer",length = 11)
    private int number_answer;

    @Column(name = "time_test")
    private Date timeTest;

    @Column(name = "time_start")
    private Date timeStart;

    @Column(name = "time_end")
    private Date timeEnd;

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @ManyToMany
    @JoinTable(name = "test_question", joinColumns = {
            @JoinColumn(name = "id_test", referencedColumnName = "id") }, inverseJoinColumns = {
            @JoinColumn(name = "id_question", referencedColumnName = "id") })
    private List<Question> questionList = new ArrayList();
    public Test() {
    }

    public Test(String testName, String description, int number_answer, Date timeTest, Date timeStart, Date timeEnd, User createBy) {
        this.testName = testName;
        this.description = description;
        this.number_answer = number_answer;
        this.timeTest = timeTest;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.createBy = createBy;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumber_answer() {
        return number_answer;
    }

    public void setNumber_answer(int number_answer) {
        this.number_answer = number_answer;
    }

    public Date getTimeTest() {
        return timeTest;
    }

    public void setTimeTest(Date timeTest) {
        this.timeTest = timeTest;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }
}
