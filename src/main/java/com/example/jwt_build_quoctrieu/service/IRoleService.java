package com.example.jwt_build_quoctrieu.service;



import com.example.jwt_build_quoctrieu.entity.Role;

import java.util.List;

public interface IRoleService {

    List<Role> findAll();

    Role findOne(Long id);
}
