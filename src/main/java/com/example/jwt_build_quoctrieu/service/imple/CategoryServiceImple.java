package com.example.jwt_build_quoctrieu.service.imple;


import com.example.jwt_build_quoctrieu.entity.Category;
import com.example.jwt_build_quoctrieu.model.CategoryModel;
import com.example.jwt_build_quoctrieu.repository.ICategoryRepository;
import com.example.jwt_build_quoctrieu.service.ICategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service(value = "iCategoryService")
public class CategoryServiceImple implements ICategoryService {
    @Resource
    ICategoryRepository categoryRepository;

    @Override
    public Page<Category> findAll(Pageable pageable) {
        return this.categoryRepository.findAll(pageable);
    }

    @Override
    public List<Category> lsCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Object insert(CategoryModel categoryModel) {
        Category category = new Category();
        category.setDescription(categoryModel.getDescription());
        category.setCreateBy(categoryModel.getCreateBy());
        category.setIs_Delete(1);
        category.setIdParent(categoryModel.getId());
        category.setCreateDate(categoryModel.getCreateDate());
        category.setCategoryName(categoryModel.getCategoryName());
        return this.categoryRepository.save(category);


    }

    @Override
    public Category update(CategoryModel categoryModel) {
        Optional<Category> categoryOtp = categoryRepository.findById(categoryModel.getId());
        Category category = new Category();
        if (categoryOtp.isPresent()) {
            Date date = new Date();
            category = categoryOtp.get();
            category.setDescription(categoryModel.getDescription());
            category.setUpdateBy(categoryModel.getUpdateBy());
            category.setUpdateDate(categoryModel.getUpdateDate());
            category.setIdParent(categoryModel.getId());
            category.setCategoryName(categoryModel.getCategoryName());
        }


        return this.categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        Optional<Category> Optcategory = findById(id);
        Category category = Optcategory.get();
        category.setIs_Delete(0);
        categoryRepository.save(category);
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }
}
