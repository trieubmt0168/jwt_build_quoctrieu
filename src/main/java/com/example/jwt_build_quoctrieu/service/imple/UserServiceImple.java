package com.example.jwt_build_quoctrieu.service.imple;

import com.example.jwt_build_quoctrieu.entity.User;
import com.example.jwt_build_quoctrieu.model.UserModel;
import com.example.jwt_build_quoctrieu.repository.IRoleRepository;
import com.example.jwt_build_quoctrieu.repository.IUserRepository;
import com.example.jwt_build_quoctrieu.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service(value = "iUserService")
public class UserServiceImple implements IUserService {
    @Autowired
    private static BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Resource
    private IUserRepository userRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Override
    public Page<User> findAll(Pageable pageable) {

        return this.userRepository.findAll(pageable);
    }

    @Override
    public List<User> lsUser() {
        return this.userRepository.findAll();
    }

    @Override
    public User insert(UserModel userModel) {

        User user = new User();
        user.setUsername(userModel.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));
        user.setName(userModel.getName());
        user.setEmail(userModel.getEmail());
        user.setPhone(userModel.getPhone());
        user.setBirthDay(userModel.getBirthday());
        user.setStatus(1);
        user.setCreateBy(userModel.getCreateBy());
        user.setCreateDate(userModel.getCreateDate());
        user.setIs_Delete(1);
        user.setRoleSet(userModel.getRoleSet());
        return this.userRepository.save(user);

    }
    @Override
    public User inserts(User user) {
        user.setUsername(user.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setName(user.getName());
        user.setEmail(user.getEmail());
        user.setPhone(user.getPhone());
        user.setBirthDay(user.getBirthDay());
        user.setStatus(1);
        user.setCreateBy(user.getCreateBy());
        user.setCreateDate(user.getCreateDate());
        user.setIs_Delete(1);
        user.setRoleSet(user.getRoleSet());
        return this.userRepository.save(user);

    }



    @Override
    public User update(UserModel userModel) {

        Optional<User> userOpt = userRepository.findById(userModel.getId());
        User user = new User();
        if (userOpt.isPresent()) {
            user = userOpt.get();
            user.setId(userModel.getId());
            user.setUsername(userModel.getUsername());
            user.setName(userModel.getName());
            user.setEmail(userModel.getEmail());
            user.setPhone(userModel.getPhone());
            user.setBirthDay(userModel.getBirthday());
            user.setStatus(userModel.getStatus());
            user.setUpdateBy(userModel.getUpdateBy());
            user.setUpdateDate(userModel.getUpdateDate());
            user.setIs_Delete(userModel.getIs_Delete());
            user.setRoleSet(userModel.getRoleSet());
        }
        return this.userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }
}
