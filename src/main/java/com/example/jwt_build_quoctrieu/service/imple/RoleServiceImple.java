package com.example.jwt_build_quoctrieu.service.imple;

import com.example.jwt_build_quoctrieu.entity.Role;
import com.example.jwt_build_quoctrieu.repository.IRoleRepository;
import com.example.jwt_build_quoctrieu.service.IRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImple implements IRoleService {
    @Resource
    private IRoleRepository roleRepository;

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role findOne(Long id) {
        return this.roleRepository.findById(id).get();
    }
}
