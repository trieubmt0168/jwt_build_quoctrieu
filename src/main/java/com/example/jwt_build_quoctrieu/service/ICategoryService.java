package com.example.jwt_build_quoctrieu.service;


import com.example.jwt_build_quoctrieu.entity.Category;
import com.example.jwt_build_quoctrieu.model.CategoryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ICategoryService {
    Page<Category> findAll(Pageable pageable);

    List<Category> lsCategory();

    Object insert(CategoryModel category);

    Category update(CategoryModel categoryModel);

    void delete(Long id);

    Optional<Category> findById(Long id);
}
