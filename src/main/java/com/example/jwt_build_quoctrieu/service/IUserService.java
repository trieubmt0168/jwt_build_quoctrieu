package com.example.jwt_build_quoctrieu.service;



import com.example.jwt_build_quoctrieu.entity.User;
import com.example.jwt_build_quoctrieu.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    Page<User> findAll(Pageable pageable);

    List<User> lsUser();

    Object insert(UserModel userModel);

    Object inserts(User user);

    User update(UserModel userModel);

    void delete(Long id);

    Optional<User> findById(Long id);
}
