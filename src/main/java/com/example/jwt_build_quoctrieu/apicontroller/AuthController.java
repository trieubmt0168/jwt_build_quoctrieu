package com.example.jwt_build_quoctrieu.apicontroller;


import com.example.jwt_build_quoctrieu.entity.ERole;
import com.example.jwt_build_quoctrieu.entity.Role;
import com.example.jwt_build_quoctrieu.entity.User;
import com.example.jwt_build_quoctrieu.payload.request.LoginRequest;
import com.example.jwt_build_quoctrieu.payload.request.SignupRequest;
import com.example.jwt_build_quoctrieu.payload.response.JwtResponse;
import com.example.jwt_build_quoctrieu.payload.response.MessageResponse;
import com.example.jwt_build_quoctrieu.repository.IRoleRepository;
import com.example.jwt_build_quoctrieu.repository.IUserRepository;
import com.example.jwt_build_quoctrieu.security.jwt.JwtUtils;
import com.example.jwt_build_quoctrieu.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IUserRepository iUserRepository;

	@Autowired
	IRoleRepository iRoleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		System.out.println(encoder.matches(loginRequest.getPassword(),"$2a$10$P132jqVOM06Y0sSVxhoXfO5xMvADcediWtYIHaUPbNINvmRjD/Bfi"));
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));


		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());


		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (iUserRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (iUserRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(),
				signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = iRoleRepository.findByName(ERole.ROLE_STUDY)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "admin":
						Role adminRole = iRoleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);

						break;
					case "user":
						Role userRole = iRoleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);

						break;
					default:
						Role studyRole = iRoleRepository.findByName(ERole.ROLE_STUDY)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(studyRole);
				}
			});
		}

		user.setRoleSet(roles);
		iUserRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
