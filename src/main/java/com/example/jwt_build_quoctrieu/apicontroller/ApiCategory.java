package com.example.jwt_build_quoctrieu.apicontroller;


import com.example.jwt_build_quoctrieu.entity.Category;
import com.example.jwt_build_quoctrieu.model.CategoryModel;
import com.example.jwt_build_quoctrieu.service.ICategoryService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/admin/category")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ApiCategory {
    @Autowired
    ICategoryService categoryService;
    Logger logger = LogManager.getLogger(ApiCategory.class);

    @GetMapping(value = "/list")
    @ResponseStatus(value = HttpStatus.OK)
    private Page<Category> listCategory(Pageable pageable) {
        return categoryService.findAll(pageable);
    }

    @PostMapping(value = "/createcatalog")
    @ResponseStatus(value = HttpStatus.OK)
    public void addCategory(@RequestBody CategoryModel category) {
        category.setCreateDate(new Date());
        categoryService.insert(category);
    }

    @PutMapping(value = "/Updatecatelog")
    @ResponseStatus(value =  HttpStatus.OK)
    public  void updateCategory(@RequestBody CategoryModel category){

        category.setUpdateDate(new Date());
        categoryService.update(category);
    }

    @PostMapping(value = "/deletecate/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteReservation(@PathVariable Long id) {

        categoryService.delete(id);
    }
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void findById(@PathVariable long id){
        categoryService.findById(id);
    }
}
