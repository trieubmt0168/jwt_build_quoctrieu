package com.example.jwt_build_quoctrieu.apicontroller;


import com.example.jwt_build_quoctrieu.entity.User;
import com.example.jwt_build_quoctrieu.model.UserModel;
import com.example.jwt_build_quoctrieu.service.IRoleService;
import com.example.jwt_build_quoctrieu.service.IUserService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping("/admin/user")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ApiUser {

    @Autowired
    private IUserService iUserService;


    @GetMapping(value = "/list")
    @ResponseStatus(value = HttpStatus.OK)
    private Page<User> listPageUser(Pageable pageable
    ) {
        Page<User> userPage = iUserService.findAll(pageable);
        return userPage;
    }

    @PostMapping(value = "/create")
    @ResponseStatus(value = HttpStatus.OK)
    public void addUser(@RequestBody UserModel user) {
        iUserService.insert(user);
    }
    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Optional<User> getReservation(@PathVariable long id) {
        return iUserService.findById(id);
    }


    @PutMapping(value = "/update")
    @ResponseStatus(value = HttpStatus.OK)
    public void updateUser(@RequestBody UserModel userModel){
        userModel.setUpdateDate(new Date());
        iUserService.update(userModel);
    }
    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUserById(@PathVariable long id){
        iUserService.delete(id);
    }
}
