package com.example.jwt_build_quoctrieu.model;



import com.example.jwt_build_quoctrieu.entity.Role;

import java.util.Date;
import java.util.Set;

public class  UserModel {

    private Long id;
    private String username;
    private String password;
    private String name;
    private String email;
    private String phone;
    private Date birthday;
    private Date createDate;
    private Date updateDate;
    private Integer status;
    private Integer is_Delete;
    private Integer createBy;
    private Integer updateBy;
    private Set<Role> roleSet;

    public UserModel() {
        super();
    }

    public UserModel(Long id, String username, String password, String name, String email, String phone, Date birthday, Date createDate, Date updateDate, Integer status, Integer is_Delete, Integer createBy, Integer updateBy, Set<Role> roleSet) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.birthday = birthday;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.status = status;
        this.is_Delete = is_Delete;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.roleSet = roleSet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIs_Delete() {
        return is_Delete;
    }

    public void setIs_Delete(Integer is_Delete) {
        this.is_Delete = is_Delete;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Set<Role> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<Role> roleSet) {
        this.roleSet = roleSet;
    }
}
