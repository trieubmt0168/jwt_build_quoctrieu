package com.example.jwt_build_quoctrieu.model;



import com.example.jwt_build_quoctrieu.entity.Category;

import java.util.Date;
import java.util.Set;


public class CategoryModel {
    private Long id;
    private String categoryName;
    private String nameParent;
    private Integer CreateBy;
    private String description;
    private Set<Category> parent_id;
    private Integer updateBy;
    private Date createDate;
    private Date updateDate;
    private boolean isDelete;
    private Long idParent;

    public CategoryModel() {
        super();
    }

    public CategoryModel(Long id, String categoryName, String nameParent, Integer createBy, String description, Set<Category> parent_id, Integer updateBy, Date createDate, Date updateDate, boolean isDelete, Long idParent) {
        this.id = id;
        this.categoryName = categoryName;
        this.nameParent = nameParent;
        CreateBy = createBy;
        this.description = description;
        this.parent_id = parent_id;
        this.updateBy = updateBy;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.isDelete = isDelete;
        this.idParent = idParent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getNameParent() {
        return nameParent;
    }

    public void setNameParent(String nameParent) {
        this.nameParent = nameParent;
    }

    public Integer getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(Integer createBy) {
        CreateBy = createBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Category> getParent_id() {
        return parent_id;
    }

    public void setParent_id(Set<Category> parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Long getIdParent() {
        return idParent;
    }

    public void setIdParent(Long idParent) {
        this.idParent = idParent;
    }
}
