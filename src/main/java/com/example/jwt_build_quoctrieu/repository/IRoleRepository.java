package com.example.jwt_build_quoctrieu.repository;


import com.example.jwt_build_quoctrieu.entity.ERole;
import com.example.jwt_build_quoctrieu.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
