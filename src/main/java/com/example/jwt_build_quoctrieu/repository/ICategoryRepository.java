package com.example.jwt_build_quoctrieu.repository;


import com.example.jwt_build_quoctrieu.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long> {
    @Query(value = "Select * from Category where is_delete=1", nativeQuery = true)
    Page<Category> findAll(Pageable pageable);
}
