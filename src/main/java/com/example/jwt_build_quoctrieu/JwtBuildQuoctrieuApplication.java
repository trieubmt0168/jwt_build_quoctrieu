package com.example.jwt_build_quoctrieu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
public class JwtBuildQuoctrieuApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtBuildQuoctrieuApplication.class, args);
    }

}
